# Pantheon

| Major Arcana   | Alignment       | Name                  | Portfolio                 |
| -------------- | --------------- | --------------------- | ------------------------- |
| Fool           | True Neutral    | [[The Voyager]]       | Adventure, New Beginnings |
| Magician       | Lawful Evil     | [[The Whispered One]] | Ambition, Secrets         |
| High Priestess | Neutral Good    | [[The West Wind]]     | The Harvest, Hospitality  |
| Empress        | Chaotic Evil    | [[The Southmother]]   | Passion, Greed            |
| Emperor        | Lawful Good     | [[The Northfather]]   | Wisdom, Truth             |
| Hierophant     | Lawful Neutral  | [[The Taleteller]]    | Law, Knowledge            |
| Lovers         | Chaotic Neutral | [[The Conductor]]     | Passion, Love             |
| Chariot        | Chaotic Neutral | [[The Messenger]]     | Omens, Subterfuge         |
| Strength       | Chaotic Good    | [[The Brightblade]]   | Strength at Arms, Battle  |
| Hermit         | Lawful Good     | [[The Meditant]]      | Outcasts, Mastery         |
| Fortune        | Neutral Good    | [[The Weaver]]        | Fortune, Luck             |
| Justice        | Lawful Neutral  | [[The Seer]]          | Justice, Retribution      |
| Hanged Man     | Chaotic Good    | [[The Eternal]]       | Perseverance, Endurance   |
| Death          | Neutral Evil    | [[The Gatekeeper]]    | Death, Transition         |
| Temperance     | Neutral Evil    | [[The Purifier]]      | Crusades, Sacrifice       |
| Devil          | Lawful Evil     | [[The Man of Glass]]  | Trickery, Domination      |
| Tower          | Neutral Evil    | [[The Kingslayer]]    | Destruction, Pain         |
| Star           | True Neutral    | [[The Watcher]]       | Balance, Foresight        |
| Moon           | Chaotic Evil    | [[The Endbringer]]    | Madness, Chaos            |
| Sun            | Chaotic Good    | [[The Harbinger]]     | Courage, Renewal          |
| Judgment       | Lawful Neutral  | [[The Arbiter]]       | Judgment, Protection      |
| World          | True Neutral    | [[The Author]]        | Fate, Destiny             |
