# The Taleteller

![[the-taleteller.jpg]]

* **Major Arcana**: The Hierophant
* **Alignment**: Lawful Neutral
* **Portfolio**: Law, Knowledge

He is the prophet, the priest, the conduit of the higher power.
Will he use this power to guide men ot their pinnacle, or their downfall?

Aureon from Eberron.
