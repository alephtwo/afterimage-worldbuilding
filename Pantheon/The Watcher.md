# The Watcher

![[the-watcher.jpg]]

* **Major Arcana**: The Star
* **Alignment**: True Neutral
* **Portfolio**: Balance, Foresight

The order of the Cosmos, the music of the spheres. Fate as is has been written,
and the natural order as it will always be.

Referred to by Ancient Estalucians as "Io."
