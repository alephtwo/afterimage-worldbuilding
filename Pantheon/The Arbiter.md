# The Arbiter

![[the-arbiter.jpg]]

* **Major Arcana**: Judgment
* **Alignment**: Lawful Neutral
* **Portfolio**: Judgment, Protection

The execution of laws and justice by men. Will the jury come to the right
conclusion as determined by natural laws (8. Justice), or will their own
emotions enact an unfair sentence?
