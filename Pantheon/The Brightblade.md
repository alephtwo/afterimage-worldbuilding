# The Brightblade

![[the-brightblade.jpg]]

* **Major Arcana**: Strength
* **Alignment**: Chaotic Good
* **Portfolio**: Strength at Arms, Battle

The strong may be the defender of the weak, the bringer of justice, or the
raiding murderers for whom might makes right.

Dol Dorn from Eberron.
