# The West Wind

![[the-west-wind.jpg]]

* **Major Arcana**: The High Priestess
* **Alignment**: Neutral Good
* **Portfolio**: The Harvest, Hospitality

She is the divine mother. Will able to bestow comfort and fortune, or lethal
divine judgement?
