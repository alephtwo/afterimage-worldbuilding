# The Messenger

![[the-messenger.jpg]]

* **Major Arcana**: The Chariot
* **Alignment**: Chaotic Neutral
* **Portfolio**: Omens, Subterfuge

The carrier of men, the agent of chance. Does it carry a messenger of good
news, a soldier into war, or strange peoples into your land?
