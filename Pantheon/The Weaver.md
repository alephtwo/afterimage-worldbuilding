# The Weaver

![[the-weaver.jpg]]

* **Major Arcana**: Wheel of Fortune
* **Alignment**: Neutral Good
* **Portfolio**: Fortune, Luck

The arbiter of fate. Will your future hold a rise to wealth, or a fall into despair?

Olladra from Eberron.
