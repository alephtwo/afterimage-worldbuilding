# The Author

![[the-author.jpg]]

* **Major Arcana**: The World
* **Alignment**: True Neutral
* **Portfolio**: Fate, Destiny

The forum of action. This is the grand theater where the above forces act out
the drama of history. What will be your role?
