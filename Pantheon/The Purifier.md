# The Purifier

![[the-purifier.jpg]]

* **Major Arcana**: Temperance
* **Alignment**: Neutral Evil
* **Portfolio**: Crusades, Sacrifice

The will to resist bad things. Maybe you will keep your house and body pure, or
maybe you will go overboard and wipe clean that which you mistakenly see as
impure (resulting in genocide).
