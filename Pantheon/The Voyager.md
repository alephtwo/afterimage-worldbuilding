# The Voyager

![[the-voyager.jpg]]

* **Major Arcana**: The Voyager
* **Alignment**: True Neutral
* **Portfolio**: Adventure, New Beginnings

He is the wandering journeyman, devoid of experience but as a blank slate he
represents true potential.
