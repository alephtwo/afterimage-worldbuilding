# The Endbringer

![[the-endbringer.jpg]]

* **Major Arcana**: The Moon
* **Alignment**: Chaotic Evil
* **Portfolio**: Madness, Chaos

The mysterious and unreachable. Will it illuminate the darkest night, or drive
you to lunacy?

This is just Tharizdun.
