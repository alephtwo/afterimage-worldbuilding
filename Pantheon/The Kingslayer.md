# The Kingslayer

![[the-kingslayer.jpg]]

* **Major Arcana**: The Tower
* **Alignment**: Neutral Evil
* **Portfolio**: Destruction, Pain

It is the bulwark, the fortress, the symbol of order and protection.
But don't rely on it too much, for it one day will fall (maybe the process
is sped up by the degeneracy of the devil).
