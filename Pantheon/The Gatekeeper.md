# The Gatekeeper

![[the-gatekeeper.jpg]]

* **Major Arcana**: Death
* **Alignment**: Neutral Evil
* **Portfolio**: Death, Transition

The end comes for all, and works through all competing things. Both for your
enemies and yourself, it cannot be forever escaped.

The Keeper from Eberron.
