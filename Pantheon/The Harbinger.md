# The Harbinger

![[the-harbinger.jpg]]

* **Major Arcana**: The Sun
* **Alignment**: Chaotic Good
* **Portfolio**: Courage, Renewal

The illuminator, provider of warmth and food. But also the cause of drought,
desert, and burns.
