# The Eternal

![[the-eternal.jpg]]

* **Major Arcana**: The Hanged Man
* **Alignment**: Chaotic Good
* **Portfolio**: Perseverance, Endurance

The is the sacrificer and the sacrificed. Was it worth it?
