# The Meditant

![[the-meditant.jpg]]

* **Major Arcana**: The Hermit
* **Alignment**: Lawful Good
* **Portfolio**: Outcasts, Mastery

He has lived a simple pious life of solitude, but what is that worth if he is
removed from society?

Majere from Dragonlance.
