# The Conductor

![[the-conductor.jpg]]

* **Major Arcana**: The Lovers
* **Alignment**: Chaotic Neutral
* **Portfolio**: Passion, Love

Their utmost devotion to one another is admirable, yet their love may cause
them to make foolish and tragic decisions.
