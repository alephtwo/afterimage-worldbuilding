# The Whispered One

![[the-whispered-one.jpg]]

* **Major Arcana**: The Magician
* **Alignment**: Lawful Evil
* **Portfolio**: Ambition, Secrets

He has spent his entire life obsessively gaining expertise in a single field.
He may go mad, or discover something which drives him to madness.

This is just Vecna.
