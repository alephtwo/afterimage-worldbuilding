# The Seer

![[the-seer.jpg]]

* **Major Arcana**: Justice
* **Alignment**: Lawful Neutral
* **Portfolio**: Justice, Retribution

The higher laws of being. Will righteousness be served, or will the pound of
flesh required be lethal to the community?
