# The Southmother

![[the-southmother.png]]

* **Major Arcana**: The Empress
* **Alignment**: Chaotic Evil
* **Portfolio**: Passion, Greed

She is the greatest influence behind the king. Will her influence aim for
virtue, or wickedness?

Referred to as "Tia" in Ancient Estalucia.
