# The Northfather

![[the-northfather.jpg]]

* **Major Arcana**: The Emperor
* **Alignment**: Lawful Good
* **Portfolio**: Wisdom, Truth

He is the lord among men. Will he bring order to his people, or tyranny?

In Ancient Estalucia, referred to as "Baha."
