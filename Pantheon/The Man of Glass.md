# The Man of Glass

![[the-man-of-glass.png]]

* **Major Arcana**: The Devil
* **Alignment**: Lawful Evil
* **Portfolio**: Trickery, Domination

A trickster, manipulator, and tempter. The voice from another man or from
inside your head, it wrongly convinces you that there is no real danger
in selfishness and degeneracy.

This is just Gaunter.
