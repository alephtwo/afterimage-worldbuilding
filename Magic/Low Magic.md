# Low Magic

Low magic is "cast" by making use of the technological remains of ancient
civilizations; most commonly by controlling environmental nanomachines,
knowingly or unknowingly.

## History

In the ancient past, Aeoril was driven to the brink of environmental collapse.
The dominant civilizations of the day produced infinitesimally small
self-assembling nanomachines which were given the sole purpose of purifying the
environment. Over time, people learned how to manipulate these machines for
their own aims; sometimes mundane things like moving earth small distances,
but also for things like instantaneous combustion.
