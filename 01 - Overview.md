# Overview

Some say that untold aeons ago, the sky was home to countless islands -- each
home to their own people and cultures. Now, only the ruins of once-great
civilizations remain.

![[northern-plains.jpg]]

## History

Thousands upon thousands of years ago, this world was known as Aeoril. None yet
live who remember this. Society has progressed and regressed countless times
since then, and the past is littered with civilizations whose names may never
be known. Their presence, however, cannot be ignored; the landscape is forever
scarred with their ruins, some clearly more advanced than others.

The world of Afterimage is set during the early days of a new golden age.
Civilization is crawling out of a dark age, and there is power to be gained by
those who seek it.

![[ruined-cathedral.jpg]]

## Magic

There are two primary types of magic: [[Low Magic]] and [[True Magic]].
